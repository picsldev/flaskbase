# flaskbase

Python > 3.5 con `pipenv`

## Para definir el entorno en la terminal

Para Windows, usar `set` en lugar de `export`.

    ssh-add

    export LC_ALL=C.UTF-8

    export LANG=C.UTF-8

## Para reproducir el proyecto

    git clone git@gitlab.com:picsldev/flaskbase.git

    cd flaskbase

    pip install pipenv

    pipenv install --dev

    cp .env.example .env

## Para lanzar el proyecto

    cd flaskbase

    pipenv shell

    flask create_all

    flask run -h 0.0.0.0 -p 9608 --with-threads

## Acceso a la aplicación

[http://127.0.0.1:9608/](http://127.0.0.1:9608/)

## Creación del primer usuario

[http://127.0.0.1:9608/register](http://127.0.0.1:9608/register)

<!-- EOF -->