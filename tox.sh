#!/usr/bin/env sh

python -m pip install --upgrade setuptools wheel pip pipenv

# Optional!
# pipenv update

tox
tox -e pep8
tox -e cover

# Optional
# python setup.py sdist bdist_wheel

# EOF
