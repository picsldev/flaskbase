import pytest

"""
Based on:
    https://www.patricksoftwareblog.com/testing-a-flask-application-using-pytest/

"""

@pytest.fixture()
def fixture01():
    print("\nIn fixture01()...")

def test_case(fixture01):
    """Test Case Example"""

    assert 'python'.upper() == 'PYTHON'

# EOF
