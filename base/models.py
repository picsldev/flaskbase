"""
[summary]

[description]

Raises:
    AttributeError -- [description]

Returns:
    [type] -- [description]
"""


from flask_login import UserMixin
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash

DB = SQLAlchemy()


class User(DB.Model, UserMixin):
    """
    [summary]

    [description]

    Arguments:
        DB {[type]} -- [description]
        UserMixin {[type]} -- [description]

    Raises:
        AttributeError -- [description]

    Returns:
        [type] -- [description]
    """

    # TODO: look and adapt this page
    # https://blog.openshift.com/use-flask-login-to-add-user-authentication-to-your-python-application/

    id = DB.Column(DB.Integer, primary_key=True)
    username = DB.Column(DB.String(64), unique=True)
    password_hash = DB.Column(DB.String(128), unique=False)
    ideas = DB.relationship('Idea', backref='user', lazy='dynamic')


    @property
    def password(self):
        """
        [summary]

        [description]

        Raises:
            AttributeError -- [description]
        """

        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        """
        [summary]

        [description]

        Arguments:
            password {[type]} -- [description]
        """

        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        """
        [summary]

        [description]

        Arguments:
            password {[type]} -- [description]

        Returns:
            [type] -- [description]
        """

        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        """
        [summary]

        [description]

        Returns:
            [type] -- [description]
        """

        return '<User %r>' % self.username


class Idea(DB.Model):
    """
    [summary]

    [description]

    Arguments:
        DB {[type]} -- [description]

    Returns:
        [type] -- [description]
    """

    id = DB.Column(DB.Integer, primary_key=True)
    name = DB.Column(DB.String(128), unique=False)
    description = DB.Column(DB.Text(), nullable=True)

    user_id = DB.Column(DB.Integer, DB.ForeignKey('user.id'))

    def __repr__(self):
        return '<Idea %r>' % self.name

# EOF
