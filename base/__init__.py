"""
[summary]

[description]

Returns:
    [type] -- [description]
"""

import os

from os.path import abspath, dirname, join

from logging.config import dictConfig
from logging.handlers import RotatingFileHandler

from flask import (Flask,
                   Blueprint,
                   render_template,
                   abort,
                   request)
# from jinja2 import TemplateNotFound  # FIXME: No used
# from flask_cors import CORS  # FIXME No used
# from flask_bootstrap import Bootstrap  # FIXME No used
from flask_login import LoginManager
from flask_wtf.csrf import CSRFProtect
from flask_debugtoolbar import DebugToolbarExtension
# from flask_mail import Mail, Message

from base.models import DB, User

# TODO: https://gist.github.com/ivanlmj/dbf29670761cbaed4c5c787d9c9c006b

# Logger level's
# CRITICAL	50
# ERROR	    40
# WARNING	30
# INFO	    20
# DEBUG 	10
# NOTSET	0

dictConfig({
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '[%(asctime)s] %(levelname)s::%(name)s::%(module)s::%(lineno)d %(message)s',
        }
    },
    'handlers': {
        'wsgi': {
            'class': 'logging.StreamHandler',
            'stream': 'ext://flask.logging.wsgi_errors_stream',
            'formatter': 'default',
            'level': 'INFO',
        },
        'file_handler': {
            'level': 'INFO',
            'formatter':'default',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': __name__ + '.log',
            'when':'midnight',
            'interval':1,
            # 'maxbytes': 10485760,
            # 'backupcount': 20,
            'encoding': 'utf8',
        },
    },
    'root': {
        'handlers': ['wsgi', 'file_handler'],
        'level': 'WARNING',
        'propagate': True
    }
})

BASEDIR = abspath(dirname(__file__))

APP = Flask(__name__)

# APP.debug = False  # flask-debugtoolbar

# ############################################################################
#
# System variables
#
# ############################################################################
APP.config['SECRET_KEY'] = os.environ['SECRET_KEY']
APP.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////%s' % join(BASEDIR,
                                                               __name__ +
                                                               '.sqlite')
APP.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = \
    os.environ['SQLALCHEMY_TRACK_MODIFICATIONS']
APP.config['DEBUG_TB_INTERCEPT_REDIRECTS'] = \
    os.environ['DEBUG_TB_INTERCEPT_REDIRECTS']
# APP.config['DEBUG_TB_INTERCEPT_REDIRECTS'] = False

# Email
# ~~~~~
APP.config['MAIL_TEST_USER'] = os.environ['MAIL_TEST_USER']
APP.config['MAIL_TEST_OTHER_USER'] = os.environ['MAIL_TEST_OTHER_USER']
APP.config['MAIL_TEST_OTHER_USER_MORE'] = os.environ['MAIL_TEST_OTHER_USER_MORE']
APP.config['MAIL_SERVER'] = os.environ['MAIL_SERVER']
APP.config['MAIL_PORT'] = os.environ['MAIL_PORT']
# APP.config['MAIL_USE_TLS'] = os.environ['MAIL_USE_TLS']
APP.config['MAIL_USE_SSL'] = os.environ['MAIL_USE_SSL']
APP.config['MAIL_USERNAME'] = os.environ['MAIL_USERNAME']
APP.config['MAIL_PASSWORD'] = os.environ['MAIL_PASSWORD']
APP.config['MAIL_DEFAULT_SENDER'] = os.environ['MAIL_DEFAULT_SENDER']

# APP.config[''] = os.environ['']

if APP.debug:
    APP.logger.debug('MAIL SERVER: {}'.format(APP.config['MAIL_SERVER']))
    APP.logger.debug('MAIL PORT: {}'.format(APP.config['MAIL_PORT']))
    # APP.logger.debug('TLS: {}'.format(APP.config['MAIL_USE_TLS']))
    APP.logger.debug('SSL: {}'.format(APP.config['MAIL_USE_SSL']))
    APP.logger.debug('USERNAME: {}'.format(APP.config['MAIL_USERNAME']))
    APP.logger.debug('PASS: {}'.format(APP.config['MAIL_PASSWORD']))
    APP.logger.debug('SENDER: {}'.format(APP.config['MAIL_DEFAULT_SENDER']))
    APP.logger.debug('DEBUG_TB_INTERCEPT_REDIRECTS: {}'.format(APP.config['DEBUG_TB_INTERCEPT_REDIRECTS']))

# ############################################################################
#
# Initialization of modules
#
# ############################################################################


DB.init_app(APP)
# BOOTSTRAP = Bootstrap(APP)  # FIXME No used
# CORS(APP)  # FIXME No used
CSRF = CSRFProtect(APP)
TOOLBAR = DebugToolbarExtension(APP)
# MAIL = Mail(APP)

LOGIN_MANAGER = LoginManager(APP)
LOGIN_MANAGER.session_protection = 'strong'
LOGIN_MANAGER.login_view = 'login'

import base.views

import base.routes


@LOGIN_MANAGER.user_loader
def load_user(user_id):
    """
    [summary]

    [description]

    Arguments:
        user_id {[type]} -- You will need to provide a user_loader
        callback. This callback is used to reload the user object from
        the user ID stored in the session. It should take the unicode ID
        of a user, and return the corresponding user object.

    Returns:
        None -- It should return None (not raise an exception) if the ID
        is not valid. (In that case, the ID will manually be removed from
        the session and processing will continue.)
    """

    return User.query.get(int(user_id))


@APP.cli.command()
def create_all():
    """
    [summary]

    [description]
    """

    DB.create_all()

# EOF
