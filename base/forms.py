"""
[summary]

[description]

Raises:
    ValidationError -- [description]
"""

from datetime import datetime

from flask_wtf import (FlaskForm, RecaptchaField)
from wtforms import (PasswordField,
                     StringField,
                     SubmitField,
                     TextField,
                     TextAreaField,
                     DateTimeField,
                     validators,
                     ValidationError)
from wtforms.validators import (EqualTo,
                                DataRequired,
                                Email,
                                InputRequired,
                                required)
from wtforms.fields.html5 import (EmailField, TelField)
from base.models import User


class RegisterForm(FlaskForm):
    """
    [summary]

    [description]

    Arguments:
        FlaskForm {[type]} -- [description]

    Raises:
        ValidationError -- [description]
    """

    username = StringField('Username', validators=[DataRequired()])
    password = \
        PasswordField('Password',
                      validators=[DataRequired(),
                                  EqualTo('password2',
                                          message='Passwords must match.')])
    password2 = PasswordField('Confirm Password', validators=[DataRequired()])
    submit = SubmitField('Register')

    def validate_username(self, field):
        """
        [summary]

        [description]

        Arguments:
            field {[type]} -- [description]

        Raises:
            ValidationError -- [description]
        """

        if User.query.filter_by(username=field.data).first():
            raise ValidationError('Username already in use.')


class LoginForm(FlaskForm):
    """
    [summary]

    [description]

    Arguments:
        FlaskForm {[type]} -- [description]
    """

    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Log In')


class AddIdeaForm(FlaskForm):
    """
    [summary]

    [description]

    Arguments:
        FlaskForm {[type]} -- [description]
    """

    name = StringField('Name', validators=[DataRequired()])
    description = TextAreaField('Description', validators=[DataRequired()])
    submit = SubmitField('Save')

# ############################################################################
#
# Contact Us Form
#
# Based on
# https://code.tutsplus.com/tutorials/intro-to-flask-adding-a-contact-page--net-28982
#
# ############################################################################


class ContactForm(FlaskForm):
    """
    [summary]

    [description]

    Arguments:
        FlaskForm {[type]} -- [description]
    """

    # name = TextField("Name",  [validators.Required()])
    name = TextField("Nombre", validators=[InputRequired('Please enter your name.')])

    # email = TextField("Email",  [validators.DataRequired(), validators.Email()])
    # email = EmailField("Email",  validators=[InputRequired('Please enter your email'), validators.Email('Please enter your email')])
    email = EmailField(label='Email',
                       validators=[InputRequired(),
                                   Email(message='Test message',
                                         # granular_message=False,
                                         # check_deliverability=False,
                                         # allow_smtputf8=True,
                                         # allow_empty_local=False
                                        )
                                   ],
                       filters=(),
                       description='Correo para poder contactar con usted, si hiciera falta.',
                       id=None,
                       default=None,
                       widget=None,
                       render_kw=None,
                       # _form=None,
                       _name=None,
                       _prefix='',
                       _translations=None,
                       # _meta=None
                       )

    # subject = TextField("Subject", [validators.Required('Please enter a title')])
    subject = TextField("Subject", validators=[validators.Required('Please enter a title')])
    message = TextAreaField("Message", [validators.Required('Please enter your text...')])
    date = DateTimeField("Date",
                         format="%Y-%m-%dT%H:%M:%S",
                         default=datetime.today, ## Now it will call it everytime.
                         validators=[validators.DataRequired()])
    # recaptcha = RecaptchaField()  # FIXME
    # https://flask-wtf.readthedocs.io/en/latest/form.html#recaptcha
    submit = SubmitField("Send")

# EOF
