"""
Project's view

[description]

Returns:
    [type] -- [description]
"""

from pprint import pprint  # FIXME: For debugger.

from flask import render_template, redirect, flash, url_for, request
from flask_login import login_required, login_user, logout_user, current_user
from flask_mail import Mail, Message

from base import APP
from base.forms import LoginForm, RegisterForm, ContactForm  # FIXME AddIdeaForm,
from base.models import DB, User  # FIXME: Idea

MAIL = Mail(APP)

@APP.route('/')
def index():
    """
    Initial route

    Load path of the homepage of the project.

    Returns:
        Boolean -- True if it's correct.
    """

    return render_template('index.html',
                           register_form=RegisterForm(),
                           login_form=LoginForm())


@APP.route('/register', methods=['GET', 'POST'])
def register():
    """
    Registration of new users.

    Form for registration of new users.

    Returns:
        Boolean -- True if it's correct.
    """

    form = RegisterForm()

    if form.validate_on_submit():
        user = User(username=form.username.data, password=form.password.data)
        DB.session.add(user)
        DB.session.commit()
        flash('Your account has been created', 'success')
        # FIXME: Once the account is created, redirect and populate the
        # login form
        return redirect(url_for('login'))
    # FIXME: make this message only appear when there is a real error
    # flash('Your account could not be created. Try again.', 'danger')
    return render_template('register.html', form=form)


@APP.route('/login', methods=['GET', 'POST'])
def login():
    """
    Route for login

    Entry login for authenticated users

    Returns:
        Boolean -- True if it's correct.
    """

    # TODO: Secure the connection, use HTTPS
    # TODO: Log the failed login attempts
    # TODO: Use captcha to prevent brute force of logins

    form = LoginForm()

    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is not None and user.verify_password(form.password.data):
            login_user(user, True)
            # return redirect(url_for('get_ideas'))
            flash('ok, I\'m logged!', 'success')
            # FIXME: Logger example
            APP.logger.info('Sample of info logger: (%d apples)', 42)
            return redirect(url_for('index'))
        flash('Invalid username or password', 'danger')
        # FIXME: Logger example
        APP.logger.info('Sample of info logger: (%d apples)', 42)
    return render_template('login.html', form=form)


@APP.route('/logout')
def logout():
    """
    [summary]

    [description]

    Returns:
        [type] -- [description]
    """

    logout_user()
    flash('You have been logged out', 'success')
    return redirect(url_for('index'))


# @APP.route('/ideas/<int:id>')
# @login_required
# def get_idea(id):
#    """
#    [summary]
#
#    [description]
#
#    Arguments:
#        id {[type]} -- [description]
#
#    Returns:
#        [type] -- [description]
#    """
#
#    idea = Idea.query.filter_by(id=id).first()
#    ideas = Idea.query.filter_by(user=current_user).all()
#
#    if idea is None:
#        return render_template('404.html'), 404
#    if idea.user != current_user:
#        return render_template('403.html'), 403
#    return render_template('idea.html', idea=idea, ideas=ideas)


# @APP.route('/ideas')
# @login_required
# def get_ideas():
#    """
#    [summary]
#
#    [description]
#
#    Returns:
#        [type] -- [description]
#    """
#
#    ideas = Idea.query.filter_by(user=current_user).all()
#    return render_template('ideas.html', ideas=ideas)


# @APP.route('/add_idea', methods=['GET', 'POST'])
# @login_required
# def add_idea():
#    """
#    [summary]
#
#    [description]
#
#    Returns:
#        [type] -- [description]
#    """
#
#    form = AddIdeaForm()
#
#    if form.validate_on_submit():
#        idea = Idea(name=form.name.data, description=form.description.data)
#        idea.user = current_user
#        DB.session.add(idea)
#        DB.session.commit()
#        flash('Your idea has been saved')
#        return redirect(url_for('get_ideas'))
#    return render_template('add_idea.html', form=form)

# ###########################################################################
#
# Error's Templates
#
# ###########################################################################

@APP.errorhandler(403)
def page_not_found(e):
    """
    [summary]

    [description]

    Arguments:
        e {[type]} -- [description]

    Returns:
        [type] -- [description]
    """

    APP.logger.warning('Error {}.'.format(e))

    return render_template('error/403.html'), 403


@APP.errorhandler(404)
def page_not_found(e):
    """
    [summary]

    [description]

    Arguments:
        e {[type]} -- [description]

    Returns:
        [type] -- [description]
    """

    APP.logger.warning('Error: {}.'.format(e))

    return render_template('error/404.html'), 404


@APP.errorhandler(410)
def page_not_found(e):
    """
    [summary]

    [description]

    Arguments:
        e {[type]} -- [description]

    Returns:
        [type] -- [description]
    """

    APP.logger.warning('Error {}.'.format(e))

    return render_template('error/410.html'), 410


@APP.errorhandler(500)
def page_not_found(e):
    """
    [summary]

    [description]

    Arguments:
        e {[type]} -- [description]

    Returns:
        [type] -- [description]
    """

    APP.logger.warning('Error {}.'.format(e))

    return render_template('error/500.html'), 500


# ###########################################################################
#
# Contact Us Form
#
# ###########################################################################

@APP.route('/contact', methods=['GET', 'POST'])
def contact():
    """[summary]
    """

    form = ContactForm()

    # return render_template('contact.html', form=form)

# ----------------------------------------------------------------------------

    # if request.method == 'POST':
    #    flash('Form posted (flash)', 'success')  # FIXME
    #    return 'Form posted.'
    # elif request.method == 'GET':
    #    return render_template('contact.html', form=form)

# ---------------------------------------------------------------------------

    if request.method == 'POST':

# ---------------------------------------------------------------------------

        if form.validate() is False:

            APP.logger.warning('Error al validar el formulario.')
            APP.logger.debug('{}'.format(form))

            if APP.debug:
                # name

                APP.logger.debug(40 * '~' + '(name)')
                APP.logger.debug(pprint(vars(form.name)))
                # APP.logger.debug(pprint(help(form.name)))
                APP.logger.debug(form.name.label)
                APP.logger.debug(form.name.data)
                # APP.logger.debug(form.name.label.__doc__)
                # -----------------------------------------------------------

                # email
                APP.logger.debug(40 * '~' + '(email)')
                APP.logger.debug(pprint(vars(form.email)))
                # APP.logger.debug(pprint(help(form.email)))
                APP.logger.debug(form.email.label)
                APP.logger.debug(form.email.data)
                # APP.logger.debug(form.email.label.__doc__)
                # -----------------------------------------------------------

                # subject
                APP.logger.debug(40 * '~' + '(subject)')
                APP.logger.debug(pprint(vars(form.subject)))
                # APP.logger.debug(pprint(help(form.subject)))
                APP.logger.debug(form.subject.label)
                APP.logger.debug(form.subject.data)
                # APP.logger.debug(form.subject.label.__doc__)
                # -----------------------------------------------------------

                # message
                APP.logger.debug(40 * '~' + '(message)')
                APP.logger.debug(pprint(vars(form.message)))
                # APP.logger.debug(pprint(help(form.message)))
                APP.logger.debug(form.message.label)
                APP.logger.debug(form.message.data)
                # APP.logger.debug(form.message.label.__doc__)
                # -----------------------------------------------------------

                # date
                APP.logger.debug(40 * '~' + '(date)')
                APP.logger.debug(pprint(vars(form.date)))
                # APP.logger.debug(pprint(help(form.date)))
                APP.logger.debug(form.date.label)
                APP.logger.debug(form.date.data)
                # APP.logger.debug(form.date.label.__doc__)
                # -----------------------------------------------------------

                # recaptcha
                # APP.logger.debug(40 * '~' + '(recaptcha)')
                # APP.logger.debug(pprint(vars(form.recaptcha)))
                # APP.logger.debug(form.recaptcha.label)
                # APP.logger.debug(form.recaptcha.label.__doc__)
                # -----------------------------------------------------------

                # submit
                APP.logger.debug(40 * '~' + '(submit)')
                APP.logger.debug(pprint(vars(form.submit)))
                # APP.logger.debug(pprint(help(form.submit)))
                APP.logger.debug(form.submit.label)
                APP.logger.debug(form.submit.data)
                # APP.logger.debug(form.submit.label.__doc__)
                # -----------------------------------------------------------

                APP.logger.debug(print(78*'~'))


            flash('TODO: form.validate() is False.', 'info')
            # flash('All fields are required.', 'danger')
            return render_template('contact.html', form=form)
        else:
            APP.logger.info('Formulario validado.')

            try:
                if APP.debug:
                    APP.logger.debug('MAIL_TEST_USER: {}'.format(APP.config['MAIL_TEST_USER']))
                    APP.logger.debug('MAIL_TEST_OTHER_USER: {}'.format(APP.config['MAIL_TEST_OTHER_USER']))
                    APP.logger.debug('MAIL_TEST_OTHER_USER_MORE: {}'.format(APP.config['MAIL_TEST_OTHER_USER_MORE']))

                msg = Message('[Mail from web] {}'.format(form.subject.data),
                              sender=APP.config['MAIL_TEST_USER'],
                              recipients=[APP.config['MAIL_TEST_OTHER_USER'],
                                          APP.config['MAIL_TEST_OTHER_USER_MORE']])

                # FIXME: use format?
                # msg.body = "Email form contact view."
                msg.body = """
                From: %s <%s>

                %s
                """ % (form.name.data, form.email.data, form.message.data)

                MAIL.send(msg)

                flash('Your mail has been send', 'success')

                return redirect(url_for('index'))
            except Exception as this_error:
                return(str(this_error))

            # return redirect('/success'))  # FIXME
            # return render_template('contact.html', form=form)
            # return '<html><head></head><body>Email send...</body></html>'

# ---------------------------------------------------------------------------

        #if form.validate_on_submit():
        #    APP.logger.info('Formulario validado.')
        #    # return redirect('/success'))  # FIXME
        #    return 'Form posted'
        #else:
        #    APP.logger.warning('Error al validar el formulario.')
        #    APP.logger.debug('{}'.format(form))
        #
        #    if APP.debug:
        #        # name
        #
        #        APP.logger.debug(40 * '~' + '(name)')
        #        APP.logger.debug(pprint(vars(form.name)))
        #        # APP.logger.debug(pprint(help(form.name)))
        #        APP.logger.debug(form.name.label)
        #        APP.logger.debug(form.name.data)
        #        # APP.logger.debug(form.name.label.__doc__)
        #        # -----------------------------------------------------------
#
        #        # email
        #        APP.logger.debug(40 * '~' + '(email)')
        #        APP.logger.debug(pprint(vars(form.email)))
        #        # APP.logger.debug(pprint(help(form.email)))
        #        APP.logger.debug(form.email.label)
        #        APP.logger.debug(form.email.data)
        #        # APP.logger.debug(form.email.label.__doc__)
        #        # -----------------------------------------------------------

        #        # subject
        #        APP.logger.debug(40 * '~' + '(subject)')
        #        APP.logger.debug(pprint(vars(form.subject)))
        #        # APP.logger.debug(pprint(help(form.subject)))
        #        APP.logger.debug(form.subject.label)
        #        APP.logger.debug(form.subject.data)
        #        # APP.logger.debug(form.subject.label.__doc__)
        #        # -----------------------------------------------------------

        #        # message
        #        APP.logger.debug(40 * '~' + '(message)')
        #        APP.logger.debug(pprint(vars(form.message)))
        #        # APP.logger.debug(pprint(help(form.message)))
        #        APP.logger.debug(form.message.label)
        #        APP.logger.debug(form.message.data)
        #        # APP.logger.debug(form.message.label.__doc__)
        #        # -----------------------------------------------------------

        #        # date
        #        APP.logger.debug(40 * '~' + '(date)')
        #        APP.logger.debug(pprint(vars(form.date)))
        #        # APP.logger.debug(pprint(help(form.date)))
        #        APP.logger.debug(form.date.label)
        #        APP.logger.debug(form.date.data)
        #        # APP.logger.debug(form.date.label.__doc__)
        #        # -----------------------------------------------------------

        #        # recaptcha
        #        # APP.logger.debug(40 * '~' + '(recaptcha)')
        #        # APP.logger.debug(pprint(vars(form.recaptcha)))
        #        # APP.logger.debug(form.recaptcha.label)
        #        # APP.logger.debug(form.recaptcha.label.__doc__)
        #        # -----------------------------------------------------------

        #        # submit
        #        APP.logger.debug(40 * '~' + '(submit)')
        #        APP.logger.debug(pprint(vars(form.submit)))
        #        # APP.logger.debug(pprint(help(form.submit)))
        #        APP.logger.debug(form.submit.label)
        #        APP.logger.debug(form.submit.data)
        #        # APP.logger.debug(form.submit.label.__doc__)
        #        # -----------------------------------------------------------

        #        APP.logger.debug(print(78*'~'))

        #    return render_template('contact.html', form=form)

# ---------------------------------------------------------------------------

    elif request.method == 'GET':

        APP.logger.info('Reentrada del formulario con datos.')

        return render_template('contact.html', form=form)


# ###########################################################################
# Test Flask-Mail
# ###########################################################################
@APP.route("/test_mail")
def test_mail():
    """
        subject='',
        recipients=None,
        body=None,
        html=None,
        sender=None,
        cc=None,
        bcc=None,
        attachments=None,
        reply_to=None,
        date=None,
        charset=None,
        extra_headers=None,
        mail_options=None,
        rcpt_options=None)
    """

    # msg = Message("Hello Worl",
    #              sender="lcabrera@pic-sl.com",
    #              recipients=["lcabrera@pic-sl.com"])

    # msg = Message(subject='Hello from Flask-Mail',
    #              recipients=['lcabrera@pic-sl.com'],
    #              body='Hello Flask message sent from Flask-Mail',
    #              sender=['desarrollo.picsl@@gmail.com'],
    #              bcc='desarrollo.picsl@gmail.com',)
    # # msg.body = "Hello Flask message sent from Flask-Mail"
    # Mail.send('a', msg)

    # msg = Message("Hello",
    #    # sender="from@example.com",
    #    recipients=["lcabrera@pic-sl.com"]
    # )
    # msg.body = "testing"
    # Mail.send('hola', msg)

    try:
        if APP.debug:
            APP.logger.debug('MAIL_TEST_USER: {}'.format(APP.config['MAIL_TEST_USER']))
            APP.logger.debug('MAIL_TEST_OTHER_USER: {}'.format(APP.config['MAIL_TEST_OTHER_USER']))
            APP.logger.debug('MAIL_TEST_OTHER_USER_MORE: {}'.format(APP.config['MAIL_TEST_OTHER_USER_MORE']))

        msg = Message('Hello from Flask',
                      sender=APP.config['MAIL_TEST_USER'],
                      recipients=[APP.config['MAIL_TEST_OTHER_USER'],
                                  APP.config['MAIL_TEST_OTHER_USER_MORE']])
        msg.body = "This is the email body..."
        MAIL.send(msg)

        return redirect(url_for('index'))
    except Exception as this_error:
        return(str(this_error))

# EOF
