#!/usr/bin/env python

"""
flaskbase
=========

Python > 3.5 con `pipenv`
-------------------------

    git clone git@gitlab.com:picsldev/flaskbase.git

    cd flaskbase

    cp .env.example .env

    pipenv install --dev

    pipenv shell

    flask create_all

    flask run -h 0.0.0.0 -p 9608 --with-threads

Acceso a la aplicación:

[http://127.0.0.1:9608/](http://127.0.0.1:9608/)  **ó**  [http://127.0.0.1:9608/register](http://127.0.0.1:9608/register)

"""

import flask

from flask_script import (Manager, Shell, Server, prompt, prompt_pass,
                          prompt_bool)

# from example import app



if __name__ == '__main__':
    flask.run()
