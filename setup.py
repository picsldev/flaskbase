import setuptools

# https://packaging.python.org/tutorials/packaging-projects/#setup-py

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="flaskbase",
    version="0.0.1",
    author="Luis",
    author_email="lcabrera@pic-sl.com",
    description="A small example package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/picsldev/flaskbase",
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ),
)
